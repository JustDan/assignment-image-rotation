/*
 * Author: Igor Zhirkov
 * Modified by: Pimenov Danila
 * Last update: 25.01.2021 15:14
 * Direction: Some utils
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err(const char* msg, ...)
{
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(404);
}
void print_err(const char* msg, ...)
{
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  vfprintf(stderr, "\n", args);
  va_end (args);
}


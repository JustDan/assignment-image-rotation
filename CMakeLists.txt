cmake_minimum_required(VERSION 3.17)
project(assignment_image_rotation C)

set(CMAKE_C_STANDARD 11)

include_directories(view-header)

add_executable(assignment_image_rotation
        main/bmp.c
        main/bmp.h
        main/main.c
        utils/util.c
        utils/util.h  main/image.h main/image.c  main/files.h main/files.c main/transforms.c main/transforms.h)

/*
 * Author: Pimenov Danila
 * Last update: 25.01.2021 15:09
 * Direction: Work with data in BMP format
 */

#include "bmp.h"

#define FOR_BMP_HEADER(FOR_FIELD) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD(t, n) t n;
#define BM 19778
#define BMINFOHEADER 40

struct __attribute__((packed)) bmp_header
{
  FOR_BMP_HEADER(DECLARE_FIELD)
};

/*
 * static bool read_header( FILE* f, struct bmp_header* header )
 * Direction: Read bmp header from the file
 * Params:
 *    FILE* f - Input file
 *    struct bmp_header - Output header
 * Return value: bool
 */
static bool read_header( FILE* f, struct bmp_header* header )
{
  return fread(header, sizeof(struct bmp_header), 1, f);
}

/*
 * static bool write_header( FILE* f, struct bmp_header* header )
 * Direction: Write bmp header into the file
 * Params:
 *    FILE* f - Output file
 *    struct bmp_header - Input header
 * Return value: bool
 */
static bool write_header( FILE* f, struct bmp_header* header )
{
  return fwrite(header, sizeof(struct bmp_header), 1, f);
}


static uint32_t padding( const uint32_t width )
{
  return (4 - ((3 * width) % 4)) % 4;
}

/*
 * enum read_status from_bmp( FILE* in, struct image* img, struct bmp_header* header )
 * Direction: Read and convert data from opened file in BMP format to internal image format
 * Params:
 *    FILE* in - Input file
 *    struct image* img - Converted image
 * Return value: enum read_status. Shows the success of the conversion.
 */
enum read_status from_bmp( FILE* in, struct image* img )
{
  struct bmp_header header = {0};
  read_header(in, &header);
  struct pixel* data;
  uint32_t mx = header.biWidth, my = header.biHeight, amount_of_bits = 0;
  if (header.biWidth <= 0 || header.biHeight <= 0 || header.bfileSize == 0)
    return READ_INVALID_HEADER;
  if (header.biBitCount != 24)
    return READ_INVALID_BITS;
  fseek(in, header.bOffBits, SEEK_SET);
  data = malloc(mx * my * sizeof(struct pixel));
  for (uint32_t i = 0; i < my; i++)
  {
    amount_of_bits += fread(data + i * mx, sizeof(struct pixel), mx, in);
    fseek(in, padding(mx), SEEK_CUR);
  }
  if (amount_of_bits * sizeof(struct pixel) != header.bfileSize - header.bOffBits - my * padding(mx))
  {
    free(data);
    return READ_CORRUPTED_BMP;
  }
  *img = image_create(mx, my, data);
  return READ_OK;
}

/*
 * static size_t to_bmp_write( FILE* out, struct image const* img )
 * Direction: Covert image from internal format  to bmp and place it into the file
 * Params:
 *    FILE* out - Output file
 *    struct image const* img - Image to convert
 * Return value: size_t. Returns the number of bits written.
 */
static size_t to_bmp_write( FILE* out, struct image const* img )
{
  size_t amount_of_bits = 0;
  const char end = 0x00;
  for (uint64_t i = 0; i < img->height; i++)
  {
      amount_of_bits += fwrite(img->data + i * img->width, sizeof(char), img->width * 3, out);
      amount_of_bits += fwrite(&end, sizeof(char), padding(img->width), out);
  }
  return amount_of_bits;
}
/*
 *
 */
struct bmp_header create_header( struct image const* img )
{
  struct bmp_header header = {0};
  header.bfType = BM;
  header.biSize = BMINFOHEADER;
  header.biPlanes = 1;
  header.biHeight = img->height;
  header.biWidth = img->width;
  header.biBitCount = 24;
  header.bOffBits = sizeof(struct bmp_header);
  header.bfileSize = header.bOffBits + img->height * (img->width * 3 + padding(img->width));
  return header;
}
/*
 * enum write_status to_bmp( FILE* out, struct image const* img, struct bmp_header* header )
 * Direction: Covert image from internal format  to bmp and place it with header into the file
 * Params:
 *    FILE* out - Output file
 *    struct image const* img - Image to convert
 * Return value: enum write_status. Shows the success of the conversion and writing.
 */
enum write_status to_bmp( FILE* out, struct image const* img )
{
  struct bmp_header header = create_header(img);
  write_header(out, &header);
  if (to_bmp_write(out, img) != header.bfileSize - sizeof(struct bmp_header))
    return WRITE_CORRUPTED_IMAGE;
  return WRITE_OK;
}
/*
 * Author: Pimenov Danila
 * Last update: 22.01.2021 17:16
 * Direction: open and close files.
 */

#ifndef _IMAGE_H
#define _IMAGE_H

#include <inttypes.h>
#include "malloc.h"

struct pixel
{
  uint8_t b, g, r;
};
struct image
{
  uint64_t width, height;
  struct pixel* data;
};

struct image image_init(void);
struct image image_create( uint64_t w, uint64_t h,  struct pixel* const data );
void image_destroy(struct image* img);

#endif

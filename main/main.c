/*
 * Author: Pimenov Danila
 * Last update: 25.01.2021 12:10
 * Main file
 */

#include "bmp.h"
#include "../utils/util.h"
#include "files.h"
#include "transforms.h"

int main( int argc, char** argv )
{
  struct image img = image_init(), new_img = image_init();

  if (argc < 3)
    err("Not enough arguments \n");
  if (argc > 3)
    err("Too many arguments \n");
  switch (file_load(argv[1], &img, from_bmp))
  {
    case LOAD_OK:
      break;
    case LOAD_WRONG_FORMAT:
      print_err("Wrong loaded file format");
      goto free;
    case LOAD_NO_SUCH_FILE_OR_DIRECTORY:
      print_err("No such file or dir");
      goto free;
  }
  new_img = image_rotate(img);
  switch (file_save(argv[2], &new_img, to_bmp))
  {
    case SAVE_OK:
      break;
    case SAVE_WRONG_FORMAT:
      print_err("Wrong img format to save");
      goto free;
    case SAVE_NO_SUCH_FILE_OR_DIRECTORY:
      print_err("No such file or dir");
      goto free;
  }

  free:
  image_destroy(&img);
  image_destroy(&new_img);
  return 0;
}

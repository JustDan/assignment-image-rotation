/*
 * Author: Pimenov Danila
 * Last update: 25.01.2021 15:54
 * Direction: work with internal image structure
 */

#include "image.h"

/*
 * struct image image_init( void )
 * Direction: Empty image initializing
 * Params: No params
 * Return value: struct image
 */
struct image image_init( void )
{
  return (struct image) {.height = 0, .width = 0, .data = NULL};
}

/*
 * struct image image_create( uint64_t w, uint64_t h, const char* const data )
 * Direction: Initialize image with some params and data
 * Params:
 *    uint64_t w - Image width
 *    uint64_t h - Image height
 *    const char* const data - input pixels array
 * Return value: struct image
 */
struct image image_create( uint64_t w, uint64_t h,  struct pixel* const data )
{
  return (struct image) {.width = w, .height = h, .data = data};
}

/*
 * void image_destroy(struct image* img)
 * Direction: Image destroy
 * Params:
 *    struct image* img - image to destroy
 * Return value: void
 */
void image_destroy( struct image* img )
{
  (*img).width = 0;
  (*img).height = 0;
  if ((*img).data != NULL)
    free((*img).data);
}

